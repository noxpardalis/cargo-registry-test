pub enum Arch {
    X86,
    X86_64,
}

impl Arch {
    pub fn from_target() -> Self {
        let target = std::env::var("TARGET").expect("TARGET not set");
        match target {
            target if target.contains("x86_64") => Self::X86_64,
            target if target.contains("i686") => Self::X86,
            target => panic!(
                "{:?} is unsupported try an architecture containing {:?}",
                target,
                ["x86_64", "i686"]
            ),
        }
    }
}
